﻿namespace wav_decoding
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            this.readFileButton = new System.Windows.Forms.Button();
            this.headerInfoBox = new System.Windows.Forms.RichTextBox();
            this.fftChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label1 = new System.Windows.Forms.Label();
            this.playSoundButton = new System.Windows.Forms.Button();
            this.StopSoundButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.fftChart)).BeginInit();
            this.SuspendLayout();
            // 
            // readFileButton
            // 
            this.readFileButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.readFileButton.Location = new System.Drawing.Point(51, 34);
            this.readFileButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.readFileButton.Name = "readFileButton";
            this.readFileButton.Size = new System.Drawing.Size(489, 59);
            this.readFileButton.TabIndex = 0;
            this.readFileButton.Text = "Wczytaj nowy plik .wav";
            this.readFileButton.UseVisualStyleBackColor = true;
            this.readFileButton.Click += new System.EventHandler(this.readFileButton_Click);
            // 
            // headerInfoBox
            // 
            this.headerInfoBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.headerInfoBox.Location = new System.Drawing.Point(51, 137);
            this.headerInfoBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.headerInfoBox.Name = "headerInfoBox";
            this.headerInfoBox.Size = new System.Drawing.Size(489, 528);
            this.headerInfoBox.TabIndex = 1;
            this.headerInfoBox.Text = "";
            this.headerInfoBox.TextChanged += new System.EventHandler(this.headerInfoBox_TextChanged);
            // 
            // fftChart
            // 
            chartArea1.Name = "ChartArea1";
            this.fftChart.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.fftChart.Legends.Add(legend1);
            this.fftChart.Location = new System.Drawing.Point(713, 137);
            this.fftChart.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.fftChart.Name = "fftChart";
            this.fftChart.Size = new System.Drawing.Size(585, 528);
            this.fftChart.TabIndex = 2;
            this.fftChart.Text = "chart1aaa";
            this.fftChart.Click += new System.EventHandler(this.fftChart_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(933, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 29);
            this.label1.TabIndex = 3;
            this.label1.Text = "Wykres FFT";
            // 
            // playSoundButton
            // 
            this.playSoundButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playSoundButton.Location = new System.Drawing.Point(51, 691);
            this.playSoundButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.playSoundButton.Name = "playSoundButton";
            this.playSoundButton.Size = new System.Drawing.Size(239, 57);
            this.playSoundButton.TabIndex = 4;
            this.playSoundButton.Text = "Odtwórz";
            this.playSoundButton.UseVisualStyleBackColor = true;
            this.playSoundButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // StopSoundButton
            // 
            this.StopSoundButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StopSoundButton.Location = new System.Drawing.Point(296, 691);
            this.StopSoundButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.StopSoundButton.Name = "StopSoundButton";
            this.StopSoundButton.Size = new System.Drawing.Size(244, 57);
            this.StopSoundButton.TabIndex = 5;
            this.StopSoundButton.Text = "Zatrzymaj";
            this.StopSoundButton.UseVisualStyleBackColor = true;
            this.StopSoundButton.Click += new System.EventHandler(this.StopSoundButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1362, 827);
            this.Controls.Add(this.StopSoundButton);
            this.Controls.Add(this.playSoundButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.fftChart);
            this.Controls.Add(this.headerInfoBox);
            this.Controls.Add(this.readFileButton);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.fftChart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button readFileButton;
        private System.Windows.Forms.RichTextBox headerInfoBox;
        private System.Windows.Forms.DataVisualization.Charting.Chart fftChart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button playSoundButton;
        private System.Windows.Forms.Button StopSoundButton;

        private string fileName;
        private System.Media.SoundPlayer player;
    }
}

