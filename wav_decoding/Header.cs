﻿using System;

namespace wav_decoding
{
    class Header
    {
        public char[] chunkId { get; set; }
        public Int32 chunkSize { get; set; }
        public char[] format { get; set; }
        public char[] subchunk1ID { get; set; }
        public Int32 subchunk1Size { get; set; }
        public Int16 audioFormat { get; set; }
        public Int16 numChanels { get; set; }
        public Int32 sampleRate { get; set; }
        public Int32 byteRate { get; set; }
        public Int16 blockAlign { get; set; }
        public Int16 bitsPerSample { get; set; }
        public char[] subchunk2ID { get; set; }
        public Int32 subchunk2Size { get; set; }

        public byte[] headerData;

        private int size = 44;

        public int getSize()
        {
            return size;
        }

        public Header(byte[] rawData)
        {
            headerData = new byte[size];
            Array.Copy(rawData, headerData, size);
            decode();
        }

        private char[] readChars(byte[] data, ref int start, int size)
        {
            char[] payload = new char[size];
            for (int i = 0; i < size; ++i)
            {
                payload[i] = (char)data[start + i];
            }
            start += size;
            return payload;
        }

        private int readLittleEndian(byte[] data, ref int start, int size)
        {
            int value = 0;
            for (int i = 0; i < size; ++i)
            {
                value |= data[start + i] << (i * 8);
            }
            start += size;
            return value;
        }

        private void decode()
        {
            int index = 0;

            chunkId = readChars(headerData, ref index, 4);
            chunkSize = readLittleEndian(headerData, ref index, 4);
            format = readChars(headerData, ref index, 4);
            subchunk1ID = readChars(headerData, ref index, 4);
            subchunk1Size = readLittleEndian(headerData, ref index, 4);
            audioFormat = (short)readLittleEndian(headerData, ref index, 2);
            numChanels = (short)readLittleEndian(headerData, ref index, 2);
            sampleRate = readLittleEndian(headerData, ref index, 4);
            byteRate = readLittleEndian(headerData, ref index, 4); 
            blockAlign = (short)readLittleEndian(headerData, ref index, 2);
            bitsPerSample = (short)readLittleEndian(headerData, ref index, 2);
            subchunk2ID = readChars(headerData, ref index, 4);
            subchunk2Size = readLittleEndian(headerData, ref index, 4);
            size = index;
        }
        

    }
}
