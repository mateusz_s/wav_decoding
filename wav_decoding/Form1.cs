﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DSP;

namespace wav_decoding
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            series = fftChart.Series.Add("FFT");
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void readFileButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();

            dlg.InitialDirectory = "c:\\";
            dlg.Filter = "wav files (*.wav)|*.wav|All files (*.*)|*.*";
            dlg.FilterIndex = 2;
            dlg.RestoreDirectory = true;

            byte[] byteArr = {};

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((dlg.OpenFile()) != null)
                    {
                        fileName = dlg.FileName;
                        byteArr = File.ReadAllBytes(Path.GetFullPath(fileName));
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Can't open file." + ex.Message);
                }
            }

            if (byteArr.Length != 0)
            {
                WavDecoder decoder = new WavDecoder(byteArr);

                headerInfoBox.Text = "Header " + fileName + "\n\n\n";
                headerInfoBox.Text += "Chunk ID: " + new string(decoder.header.chunkId) + "\n";
                headerInfoBox.Text += "Chunk size: " + decoder.header.chunkSize + "\n";
                headerInfoBox.Text += "Format: " + new string(decoder.header.format) + "\n";
                headerInfoBox.Text += "Subchunk1 ID: " + new string(decoder.header.subchunk1ID) + "\n";
                headerInfoBox.Text += "Subchunk size: " + decoder.header.subchunk1Size.ToString() + "\n";
                headerInfoBox.Text += "Audio format: " + decoder.header.audioFormat.ToString() + "\n";
                headerInfoBox.Text += "Number of channels: " + decoder.header.numChanels.ToString() + "\n";
                headerInfoBox.Text += "Sample rate: " + decoder.header.sampleRate.ToString() + "\n";
                headerInfoBox.Text += "Byte rate: " + decoder.header.byteRate.ToString() + "\n";
                headerInfoBox.Text += "Block align: " + decoder.header.blockAlign.ToString() + "\n";
                headerInfoBox.Text += "Bits per sample: " + decoder.header.bitsPerSample.ToString() + "\n";
                headerInfoBox.Text += "Subchunk2 ID: " + new string(decoder.header.subchunk2ID) + "\n";
                headerInfoBox.Text += "Subchunk2 size: " + decoder.header.subchunk2Size + "\n";

                var leftChannel = decoder.getChannelData(0);
                var musicDataRe = Fft.generateComplex(leftChannel, 1024);
                var musicDataIm = new double[1024];

                Fft.TransformRadix2(musicDataRe, musicDataIm);

                double x = 0;
                double freqLimit = (1 - 1 / (double)musicDataRe.Length) * decoder.header.sampleRate;
                freqLimit = freqLimit / 2;
                series.Points.Clear();

                for (int i = 1; x < freqLimit; ++i)
                {
                    var data = Math.Sqrt(musicDataRe[i] * musicDataRe[i] + musicDataIm[i] * musicDataIm[i]);
                    x += (1 / (double)musicDataRe.Length) * decoder.header.sampleRate;
                    series.Points.AddXY(x, data);
                }

            }

        }

        private void headerInfoBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (fileName != null)
            {
                player = new System.Media.SoundPlayer(fileName);

                player.Play();
            }
        }

        private void StopSoundButton_Click(object sender, EventArgs e)
        {
            if (player != null)
            {
                player.Stop();
            }
        }

        private void fftChart_Click(object sender, EventArgs e)
        {

        }
        System.Windows.Forms.DataVisualization.Charting.Series series;
    }
}
