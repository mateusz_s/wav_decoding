﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wav_decoding
{
    class WavDecoder
    {
        private byte[] data;
        public Header header;

        public WavDecoder(byte[] rawData)
        {
            header = new Header(rawData);
            var startIndex = header.getSize();

            data = new byte[rawData.Length];
            Array.Copy(rawData, startIndex, data, 0, rawData.Length - startIndex);

            audioData = new double[header.numChanels, (rawData.Length - startIndex / header.blockAlign)];
            decode();
        }

        public void decode()
        {
            if (header.bitsPerSample == 8)
            {
                for (int i = 0; i < (header.subchunk2Size / header.blockAlign); ++i) 
                {
                    audioData[i % header.numChanels, i] = ((double)data[i]/255);
                }
            }

            if (header.bitsPerSample == 16)
            {
                int arrayIndex = 0;
                for (int i = 0; i < (header.subchunk2Size / 2); i += 2)
                {
                    var channelIndex = i / 2 % header.numChanels;
                    var calculatedValue = (double)((short)((data[i] & 0x00ff) | (data[i + 1] << 8))) / 32768;
                    audioData[channelIndex, arrayIndex] = calculatedValue;
                    if (i/2 % header.numChanels == header.numChanels - 1)
                    {
                        ++arrayIndex;
                    }
                }
            }
        }

        public double[] getChannelData(int nrOfChannel)
        {
            var size = audioData.GetLength(1);
            double[] channel = new double[size];

            for (int i = 0; i < size; ++i)
            {
                channel[i] = audioData[nrOfChannel, i];
            }

            return channel;
        }

        double[,] audioData;

    }
}
